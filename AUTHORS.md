# Zesje authors

* Anton Akhmerov
* Joseph Weston
* Thomas Roos
* Justin van der Krieken
* Jamy Mahabier
* Nick Cleintuar
* Hugo Kerstens
* Stefan Hugtenburg
* Hidde Leistra
* Pim Otte
* Luc Enthoven

<!--
Execute
git shortlog -s | sed -e "s/^ *[0-9\t ]*//"| xargs -i sh -c 'grep -q "{}" AUTHORS.md || echo "{}"'

To check if any authors are missing from this list.
 -->
